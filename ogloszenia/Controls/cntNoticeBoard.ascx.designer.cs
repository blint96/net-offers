﻿//------------------------------------------------------------------------------
// <generowany automatycznie>
//     Ten kod został wygenerowany przez narzędzie.
//
//     Modyfikacje tego pliku mogą spowodować niewłaściwe zachowanie i zostaną utracone
//     w przypadku ponownego wygenerowania kodu. 
// </generowany automatycznie>
//------------------------------------------------------------------------------

namespace NoticeBoard.Controls {
    
    
    public partial class cntNoticeBoard {
        
        /// <summary>
        /// Kontrolka cntModal.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::NoticeBoard.Controls.cntModal cntModal;
        
        /// <summary>
        /// Kontrolka fw.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FormView fw;
        
        /// <summary>
        /// Kontrolka ListView2.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView ListView2;
        
        /// <summary>
        /// Kontrolka SearchTextBox.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchTextBox;
        
        /// <summary>
        /// Kontrolka BtnSearchBarPanel.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel BtnSearchBarPanel;
        
        /// <summary>
        /// Kontrolka btn_search.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn_search;
        
        /// <summary>
        /// Kontrolka ListView1.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView ListView1;
        
        /// <summary>
        /// Kontrolka AjaxSearchBar.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel AjaxSearchBar;
        
        /// <summary>
        /// Kontrolka global_notices_data.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource global_notices_data;
        
        /// <summary>
        /// Kontrolka my_notices_data.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource my_notices_data;
        
        /// <summary>
        /// Kontrolka modal_actions_data.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource modal_actions_data;
    }
}
