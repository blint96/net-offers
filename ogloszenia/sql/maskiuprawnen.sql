USE [TablicaOgloszen]
GO

/****** Object:  Table [dbo].[MaskiUprawnien]    Script Date: 07.08.2016 21:00:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MaskiUprawnien](
	[Id] [nvarchar](50) NOT NULL,
	[showQueue] [bit] NOT NULL CONSTRAINT [DF_MaskiUprawnien_showQueue]  DEFAULT ((0)),
 CONSTRAINT [PK_MaskiUprawnien] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

