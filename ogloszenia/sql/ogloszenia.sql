USE [TablicaOgloszen]
GO

/****** Object:  Table [dbo].[Ogloszenia]    Script Date: 07.08.2016 21:00:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Ogloszenia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tytul] [nvarchar](100) NOT NULL,
	[Kategoria] [int] NOT NULL,
	[Cena] [int] NOT NULL,
	[Opis] [nvarchar](250) NOT NULL,
	[Uzytkownik] [nvarchar](50) NOT NULL,
	[DataDodania] [datetime] NOT NULL,
	[DataZakonczenia] [datetime] NOT NULL,
	[Wyroznione] [bit] NOT NULL CONSTRAINT [DF_Ogloszenia_Wyroznione]  DEFAULT ((0)),
	[Status] [int] NOT NULL CONSTRAINT [DF_Ogloszenia_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_Ogloszenia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

