USE [TablicaOgloszen]
GO

/****** Object:  Table [dbo].[Uzytkownicy]    Script Date: 07.08.2016 21:00:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Uzytkownicy](
	[Id] [nvarchar](50) NOT NULL,
	[NazwaUzytkownika] [nvarchar](50) NOT NULL,
	[PoziomDostepu] [int] NOT NULL CONSTRAINT [DF_Uzytkownicy_PoziomDostepu]  DEFAULT ((0)),
 CONSTRAINT [PK_Uzytkownicy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

